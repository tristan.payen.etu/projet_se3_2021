#####################################################
CC=gcc # compilateur
CFLAGS=-W -Wall -Wextra -ggdb -Og -I include# warnings
EXTRA_CFLAGS=-fprofile-arcs -ftest-coverage # utile pour calculer la couverture de code
EXTRA_LDFLAGS=--coverage # edition de liens avec couverture de code

#### Variables liées à la compilation des sources
OBJ=build/load.o build/main.o build/fonctions.o
EXEC=projet_pa

########### Compilation du code principal ###########
all: $(EXEC)

$(EXEC): $(OBJ)
	$(CC) -o $(EXEC) $(OBJ)

build/main.o: src/main.c include/load.h build
	$(CC) $(CFLAGS) -c $< -o $@

build/%.o: src/%.c include/%.h build
	$(CC) $(CFLAGS) -c $< -o $@

build:
	mkdir build

clean:
	rm -rf *.o
	rm -rf build

mrproper: clean
	rm -rf $(EXEC)
#####################################################
