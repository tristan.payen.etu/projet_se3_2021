# Tutorat de Programmation Avancée (SE3 - 2020/2021)
# CHAUVEAU Elizabeth / PAYEN Tristan

## Résumé

L'objectif de ce projet est de vérifier que l'on maîtrise les principes vus en
cours de programmation avancée : structures de données complexes,
lecture / écriture de fichiers, compilation séparée et automatique, utilisation
de gestionnaire de version...

Pour cela, le travail demandé est de réaliser une application qui permet d'analyser 58592 vols aux États Unis en 2014. Votre travail est de charger ces fichiers pour effectuer un certain nombre de requêtes (lister des vols selon plusieurs critères, lister l'aéroligne avec plus de retards, trouver un itinéraire, ...)

## Contexte

Les données sont stockées dans trois fichiers `CSV` (_comma-separated values_) qui est un format texte permettant de stocker des tableaux. Chaque ligne du fichier correspond à une ligne du tableau et les différents éléments d'une ligne sont
séparés par un élément particulier (en général une virgule `,` mais d'autres sont possibles `\t`, `;`...). La première ligne sert à décrire le nom des différents champs.
Par exemple, le fichier `flights.csv`, qui contient les vols, a la structure suivante :

~~~
MONTH,DAY,WEEKDAY,AIRLINE,ORG_AIR,DEST_AIR,SCHED_DEP,DEP_DELAY,AIR_TIME,DIST,SCHED_ARR,ARR_DELAY,DIVERTED,CANCELLED
1,1,4,WN,LAX,SLC,1625,58.0,94.0,590,1905,65.0,0,0
1,1,4,UA,DEN,IAD,823,7.0,154.0,1452,1333,-13.0,0,0
1,1,4,MQ,DFW,VPS,1305,36.0,85.0,641,1453,35.0,0,0
1,1,4,AA,DFW,DCA,1555,7.0,126.0,1192,1935,-7.0,0,0
1,1,4,WN,LAX,MCI,1720,48.0,166.0,1363,2225,39.0,0,0
~~~
Les premiers trois champs correspondent aux mois (`1` ou janvier), jour (`1`) et jour de la semaine (`4` ou mercredi), le quatrième à la compagnie aérienne suivant les codes IATA (`WN` ou Southwest Airlines Co.), `ORG_AIR` correspond à l'aéroport d'origine ou départ (`LAX`), `DEST_AIR` correspond à l'aéroport destination ou d'arrivée (`SLC`), `SCHED_DEP` correspond à l'heure de départ prévu (`1625`), `DEP_DELAY` correspond au délais de départ en minutes (`58.0`), `AIR_TIME` correspond à la durée du vol en minutes (`94.0`), `DIST` correspond à la distance en miles (`590`), `SCHED_ARR` correspond à l'heure d'arrivée prévu (`1905`), `ARR_DELAY` correspond au retard à l'arrivée en minutes (`65.0`), `DIVERTED` est un booléen qui indique si le vol a été déviée (`0`), et `CANCELLED` est un booléen qui indique si le vol a été annulé (`0`).



De la même façon, le fichier `airports.csv` décrit les aéroports et villes correspondent aux code IATA de la façon suivante :
```
IATA_CODE,AIRPORT,CITY,STATE,COUNTRY,LATITUDE,LONGITUDE
ABE,Lehigh Valley International Airport,Allentown,PA,USA,40.652359999999994,-75.4404
ABI,Abilene Regional Airport,Abilene,TX,USA,32.41132,-99.6819
ABQ,Albuquerque International Sunport,Albuquerque,NM,USA,35.04022,-106.60918999999998
```

ainsi que le fichier `airlines.csv` qui contient les codes IATA pour chaque compagnie aérienne :
```
IATA_CODE,AIRLINE
UA,United Air Lines Inc.
AA,American Airlines Inc.
US,US Airways Inc.
```


## Cahier des charges

Il nous est demandé de réaliser un programme qui charge les fichiers de données `CSV` (ils sont disponibles dans le répertoire `data`) et qui permet de les interroger facilement.

Notre programme fonctionne de la manière suivante :

1. charger le fichier de données
2. attendre une commande
3. traiter la commande
4. afficher le résultat de cette commande
5. revenir à l'étape 2

Les commandes sont les suivantes:


- `show-airports <airline_id>`  : liste tous les aéroports depuis lesquels la compagnie aérienne `<airline_id>` opère des vols
- `show-airlines <port_id>`: liste l'ensemble des compagnie aériennes depuis l'aéroport passé en paramètre
- `show-flights <port_id> <date> <time> limit=xx` : lister les vols qui partent de l'aéroport à la date, avec optionnellement une heure de début, et limité à xx vols
- `most-delayed-flights`     : donne les 5 vols qui ont subis les plus longs retards à l'arrivée
- `most-delayed-airlines`    : donne les 5 compagnies aériennes qui ont, en moyenne, le plus de retards
- `delayed-airline <airline_id>`    : donne le retard moyen de la compagnie aérienne passée en paramètre
- `changed-flights <date>` : les vols annulés ou déviés à la date <date> (format M-D)
- `avg-flight-duration <port_id> <port_id>`: calcule le temps de vol moyen entre deux aéroports
- `quit`       : quit


Ainsi notre exécutable s'appelle `projet_pa` et on peut l'utiliser de la manière suivante:

~~~
$ ./projet_pa < data/requetes.txt
~~~

avec le fichier `requetes.txt` qui contient :

~~~
show-airports HA
show-airlines LAX
show-flights SLC 4-17 1600 limit=5
avg-flight-duration LAX JFK
most-delayed-flights
delayed-airline AA
most-delayed-airlines
changed-flights 5-15
~~~

## Précisions sur les requêtes

### `show-airports <airline_id>`
> Exemple
~~~
> show-airports HA
KOA, Kona International Airport at Keahole, Kailua/Kona, HI
LIH, Lihue Airport, Lihue, HI
SFO, San Francisco International Airport, San Francisco, CA
PHX, Phoenix Sky Harbor International Airport, Phoenix, AZ
LAS, McCarran International Airport, Las Vegas, NV
HNL, Honolulu International Airport, Honolulu, HI
OGG, Kahului Airport, Kahului, HI
LAX, Los Angeles International Airport, Los Angeles, CA
~~~

### `show-airlines <port_id>`
> Exemple
~~~
> show-airlines LAX
MQ, American Eagle Airlines Inc.
F9, Frontier Airlines Inc.
HA, Hawaiian Airlines Inc.
VX, Virgin America
B6, JetBlue Airways
NK, Spirit Air Lines
AS, Alaska Airlines Inc.
US, US Airways Inc.
AA, American Airlines Inc.
UA, United Air Lines Inc.
OO, Skywest Airlines Inc.
DL, Delta Air Lines Inc.
WN, Southwest Airlines Co.
~~~

### `show-flights <port_id> <date> <time> limit=xx`
> Exemple
~~~
> show-flights ATL 2-26 1040 limit=5
2,26,4,DL,ATL,GRR,2040,1.000,87.000,640,2240,-11.000,0,0
2,26,4,DL,ATL,RDU,1500,-2.000,48.000,356,1623,-14.000,0,0
2,26,4,DL,ATL,MLB,1526,10.000,70.000,442,1657,26.000,0,0
2,26,4,DL,ATL,TYS,1241,6.000,31.000,152,1331,2.000,0,0
2,26,4,NK,ATL,IAH,1545,58.000,125.000,689,1708,61.000,0,0
~~~

### `most-delayed-flights`
> Exemple
~~~
> most-delayed-flights
4,30,4,DL,ATL,HNL,1050,-3.000,539.000,4502,1504,-53.000,0,0
1,10,6,AS,PHX,ANC,1830,-13.000,315.000,2552,2245,-57.000,0,0
11,17,2,DL,ATL,HNL,1050,-7.000,533.000,4502,1554,-57.000,0,0
2,18,3,UA,LAX,LIH,1617,-11.000,307.000,2615,2025,-58.000,0,0
12,21,1,AA,PHX,HNL,1505,-3.000,339.000,2917,1857,-60.000,0,0
~~~

### `most-delayed-airlines`
> Exemple
~~~
> most-delayed-airlines
OO, Skywest Airlines Inc. : 7.41 minutes
UA, United Air Lines Inc. : 7.65 minutes
B6, JetBlue Airways : 8.64 minutes
F9, Frontier Airlines Inc. : 13.51 minutes
NK, Spirit Air Lines : 18.07 minutes
~~~

### `delayed-airline <airline_id>`
> Exemple
~~~
> `delayed-airline AA`
AA : 5.43 minutes
~~~

### `changed-flights <date>`
> Exemple
~~~
> changed-flights 12-29
12,29,2,EV,ORD,DSM,1552,0.000,0.000,299,1712,0.000,0,1
12,29,2,MQ,ORD,GRB,837,0.000,0.000,173,935,0.000,0,1
12,29,2,UA,IAH,SEA,2133,0.000,0.000,1874,29,0.000,0,1
12,29,2,AA,LAX,ORD,600,0.000,0.000,1744,1213,0.000,0,1
12,29,2,OO,ORD,ICT,2025,10.000,0.000,588,2229,0.000,1,0
12,29,2,AA,ORD,MCO,921,0.000,0.000,1005,1306,0.000,0,1
12,29,2,UA,ORD,SNA,1922,176.000,0.000,1726,2155,0.000,1,0
12,29,2,EV,ORD,DAY,1040,0.000,0.000,240,1254,0.000,0,1
~~~

### `avg-flight-duration <port_id> <port_id>`
> Exemple
~~~
> avg-flight-duration LAX SFO
average: 53.712231 minutes (834 flights)
~~~
