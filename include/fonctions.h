#ifndef __FONCTIONS_H__
#define __FONCTIONS_H__

#include "structures1.h"

int est_dans_la_liste (struct cellairport *, char *);

int est_dans_la_liste_airline (struct cellairline *, char *);

void show_airlines (struct cellflight **, struct airline **, char *);

void afficher_liste_airport (struct cellairport *);

void afficher_liste_airline (struct cellairline *);

void show_airports (struct cellflight **, struct cellairport **, char *);

void affiche_flights (struct flight *);

void show_flights (struct cellflight **, char *, int, int, int, int);

void changed_flights (struct cellflight **, int, int);

void avg_flight_duration (struct cellflight **, char *, char *);

void ajout_tete_flight (struct cellflight **, struct flight *);

void ajout_tete_airport (struct cellairport **, struct airport *);

void ajout_tete_airline (struct cellairline **, struct airline *);

int taille (struct cellflight *);

void supp_tete (struct cellflight **);

void afficher_liste (struct cellflight *);

void insertion_triee (struct cellflight **, struct flight *);

void most_delayed_flights (struct cellflight **);

float delayed_airline (struct cellflight **, char *, int);

void most_delayed_airlines (struct cellflight **, struct airline **);

void afficher_mda (struct cellairline *, struct cellflight **);

#endif
