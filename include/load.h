#ifndef __LOAD_H__
#define __LOAD_H__

#include "structures1.h"

int hash_date (int, int);

char hash_airport (char *);

void ajout_tete_date (struct cellflight **, struct flight *);

void ajout_tete_airport (struct cellairport **, struct airport *);

void put_flight (struct cellflight **, struct flight *);

void put_airport (struct cellairport **, struct airport *);

void put_airline (struct airline **, struct airline *, int);

void store_flights (int, char *, struct flight *);

void store_airports (int, char *, struct airport *);

void load_date (struct cellflight **);

void load_airports (struct cellairport **);

void remplace (char *);

void load_airline (struct airline **);

void lib_flights (struct cellflight **);

void suppr_tete_airports (struct cellairport **);

void lib_airports (struct cellairport **);

#endif
