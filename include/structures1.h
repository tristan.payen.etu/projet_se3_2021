#ifndef __STRUCTURES1_H__
#define __STRUCTURES1_H__

#define T 50

struct flight
{
    int   month;
    int   day;
    int   weekday;
    char  airline[T];  // compagnie aérienne
    char  org_air[T];  // aéroport de départ
    char  dest_air[T]; // aéroport d'arrivée
    int   sched_dep;   // heure de départ prévue
    float dep_delay;   // délais départ en minutes
    float air_time;    // durée vol (en min)
    int   dist;        // distance vol (miles)
    int   sched_arr;   // heure d'arrivée prévue
    float arr_delay;   // retard arrivée (en min)
    int   diverted;    // vol dévié
    int   cancelled;   // vol annulé
};

struct airport
{
    char  iata_code[T];
    char  airport[T]; // nom aéroport
    char  city[T];
    char  state[T];
    char  country[T];
    float lattitude;
    float longitude;
};

struct airline
{ // compagnie aérienne
    char iata_code[T];
    char airline[T]; // nom compagnie
};

struct cellflight
{
    struct flight *    vol;
    struct cellflight *suivant;
};

struct cellairport
{
    struct airport *    aeroport;
    struct cellairport *suivant;
};

struct cellairline
{
    struct airline *    airl;
    struct cellairline *suivant;
};

#endif
