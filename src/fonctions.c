// requêtes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "fonctions.h"
#include "load.h"

#define FLIGHT_NUMBER 58492
#define NB_VALUE_PER_LINE 14
#define NBR_DATE 365
#define NBR_LETTER 26
#define NBR_AIRLINES 14
#define AIRPORT_NUMBER 322

void affiche_flights (struct flight *vol)
{
    printf ("%d,%d,%d,%s,%s,%s,%d,%.3f,%.3f,%d,%d,%.3f,%d,%d \n", vol->month, vol->day,
            vol->weekday, vol->airline, vol->org_air, vol->dest_air, vol->sched_dep, vol->dep_delay,
            vol->air_time, vol->dist, vol->sched_arr, vol->arr_delay, vol->diverted, vol->cancelled);
}

//---------------------------------------
// SHOW AIRPORTS
//---------------------------------------
int est_dans_la_liste (struct cellairport *l, char *chaine) // retourne 1 si la chaine testée (iata_code) est deja dans la liste
{
    if (l == NULL) return 0;
    while (l != NULL)
    {
        if (strcmp (chaine, l->aeroport->iata_code) == 0)
        {
            return 1;
        }
        l = l->suivant;
    }
    return 0;
}

void afficher_liste_airport (struct cellairport *l)
{
    while (l != NULL)
    {
        printf ("%s, %s, %s, %s \n", l->aeroport->iata_code, l->aeroport->airport,
                l->aeroport->city, l->aeroport->state);
        l = l->suivant;
    }
}

void show_airports (struct cellflight *tab_flight[], struct cellairport *tab_airport[], char *airline)
{
    struct cellairport *res = NULL;
    for (int i = 0; i < NBR_DATE; i++) // on parcourt la table de hachage des vols
    {
        struct cellflight *actuel1 = tab_flight[i];
        while (actuel1 != NULL)
        {
            if (strcmp (actuel1->vol->airline, airline) == 0) // on ne s'interesse qu'aux vols de la compagnie aérienne choisie
            {
                for (int j = 0; j < AIRPORT_NUMBER; j++) // on parcourt la table de hachage des aéroports
                {
                    struct cellairport *actuel2 = tab_airport[j];
                    while (actuel2 != NULL)
                    {
                        // pour chaque vol de la compagnie on teste si le depart ou l'arrivée correspond à l'aeroport actuel
                        if ((strcmp (actuel1->vol->org_air, actuel2->aeroport->iata_code) == 0) ||
                            (strcmp (actuel1->vol->dest_air, actuel2->aeroport->iata_code) == 0))
                        {
                            if (est_dans_la_liste (res, actuel2->aeroport->iata_code) ==
                                0) // on vérifie que l'aéroport n'a pas déjà été mémorisé (pas de doublons)
                            {
                                ajout_tete_airport (&res, actuel2->aeroport); // on ajoute dans la liste de résultats l'aéroport
                            }
                        }
                        actuel2 = actuel2->suivant;
                    }
                }
            }
            actuel1 = actuel1->suivant;
        }
    }
    afficher_liste_airport (res); // on affiche les aéroports concernés
}

//---------------------------------------
// SHOW AIRLINES
//---------------------------------------
void ajout_tete_airline (struct cellairline **pl, struct airline *a)
{
    struct cellairline *new = calloc (1, sizeof (struct cellairline));
    new->airl               = a;
    new->suivant            = *pl;
    *pl                     = new;
}

void afficher_liste_airline (struct cellairline *l)
{
    while (l != NULL)
    {
        printf ("%s, %s \n", l->airl->iata_code, l->airl->airline);
        l = l->suivant;
    }
}

int est_dans_la_liste_airline (struct cellairline *l, char *chaine) // retourne 1 si la chaine testée est deja dans la liste
{
    if (l == NULL) return 0;
    while (l != NULL)
    {
        if (strcmp (chaine, l->airl->iata_code) == 0)
        {
            return 1;
        }
        l = l->suivant;
    }
    return 0;
}

void show_airlines (struct cellflight *tab_flight[], struct airline **tab_air, char *aerop)
{
    struct cellairline *res = NULL;
    for (int i = 0; i < NBR_DATE; i++) // on parcourt la table de hachage des vols
    {
        struct cellflight *actuel = tab_flight[i];
        while (actuel != NULL)
        {
            if (strcmp (actuel->vol->org_air, aerop) == 0) // que les vols qui ont le bon départ
            {
                for (int j = 0; j < NBR_AIRLINES; j++)
                {
                    // on parcours la table des airlines, quand c'est celle du vol actuel, et si elle n'est pas dans la liste, on la retient
                    if ((strcmp (tab_air[j]->iata_code, actuel->vol->airline) == 0) &&
                        (est_dans_la_liste_airline (res, tab_air[j]->iata_code) == 0))
                    {
                        ajout_tete_airline (&res, tab_air[j]); // on stocke dans une liste pour éviter les doublons
                    }
                }
            }
            actuel = actuel->suivant;
        }
    }
    afficher_liste_airline (res);
}

//---------------------------------------
// SHOW FLIGHTS
//---------------------------------------
void show_flights (struct cellflight *tab_flight[], char *aeroport, int mois, int jour, int time, int limit)
{
    int                a   = hash_date (mois, jour); // on "colle" la date
    int                cpt = 0;
    struct cellflight *actuel =
    tab_flight[a]; // on ne s'intéresse qu'aux vols partants à la date concernée (pas besoin de parcourir toute la table)
    while (actuel != NULL)
    {
        if ((strcmp (aeroport, actuel->vol->org_air) == 0) && (cpt < limit) && (actuel->vol->sched_dep >= time))
        {
            // on affiche les "limit" premiers vols respectant les conditions
            affiche_flights (actuel->vol);
            cpt++;
        }
        actuel = actuel->suivant;
    }
}

//-------------------------------------
// CHANGED FLIGHTS
//---------------------------------------
void changed_flights (struct cellflight *tab_flight[], int mois, int jour)
{
    int                a = hash_date (mois, jour); // on "colle" la date
    struct cellflight *actuel =
    tab_flight[a]; // on ne s'intéresse qu'aux vols partants à la date concernée (pas besoin de parcourir toute la table)
    while (actuel != NULL)
    {
        if ((actuel->vol->diverted == 1) || (actuel->vol->cancelled == 1))
        {
            affiche_flights (actuel->vol);
        }
        actuel = actuel->suivant;
    }
}

//---------------------------------------
// AVG FLIGHT DURATION
//---------------------------------------
void avg_flight_duration (struct cellflight *tab_flight[], char *a1, char *a2)
{
    float somme = 0;
    float cpt   = 0;
    for (int i = 0; i < NBR_DATE; i++) // on parcourt la table de hachage des vols
    {
        struct cellflight *actuel = tab_flight[i];
        while (actuel != NULL)
        {
            // on prend en compte les vols dans les deux sens
            if (((strcmp (actuel->vol->org_air, a1) == 0) && (strcmp (actuel->vol->dest_air, a2) == 0)) ||
                ((strcmp (actuel->vol->org_air, a2) == 0) && (strcmp (actuel->vol->dest_air, a1) == 0)))
            {
                somme += actuel->vol->air_time;
                cpt += 1;
            }
            actuel = actuel->suivant;
        }
    }
    printf ("average: %f minutes (%.0f flights) \n", somme / cpt, cpt);
}
//----------------------------------------
// MOST DELAYED FLIGHTS
//---------------------------------------
int taille (struct cellflight *l) // compte le nombre d'éléments d'une liste chainée
{
    if (l == NULL) return 0;
    int size = 0;
    while (l != NULL)
    {
        size++;
        l = l->suivant;
    }
    return size;
}

void supp_tete (struct cellflight **pl)
{
    if (*pl == NULL)
    {
        return;
    }
    if ((*pl)->suivant == NULL)
    {
        struct cellflight *tmp = (*pl);
        (*pl)                  = NULL;
        free (tmp);
        return;
    }
    struct cellflight *tmp = *pl;
    *pl                    = (*pl)->suivant;
    tmp->suivant           = NULL;
    free (tmp);
}

void afficher_liste (struct cellflight *l)
{
    while (l != NULL)
    {
        affiche_flights (l->vol);
        l = l->suivant;
    }
}

void ajout_tete_flight (struct cellflight **pl, struct flight *f)
{
    struct cellflight *new = calloc (1, sizeof (struct cellflight));
    new->vol               = f;
    new->suivant           = *pl;
    *pl                    = new;
}

void insertion_triee (struct cellflight **pl, struct flight *f) // ajout dans une liste chainée dans l'ordre croissant
{
    if ((*pl == NULL) || ((*pl)->vol->arr_delay < f->arr_delay))
    {
        ajout_tete_flight (pl, f);
    }
    else
    {
        insertion_triee (&(*pl)->suivant, f);
    }
}

void most_delayed_flights (struct cellflight *tab_flight[])
{
    struct cellflight *liste5 = NULL;
    for (int i = 0; i < NBR_DATE; i++) // on parcourt la table de hachage des vols
    {
        struct cellflight *actuel = tab_flight[i];
        while (actuel != NULL)
        {
            insertion_triee (&liste5, actuel->vol); // liste triée dans l'ordre croissant
            if (taille (liste5) > 5)                // on garde que les 5 plus grands
            {
                supp_tete (&liste5); // on supprime le premier, et donc le plus petit élément de la liste
            }
            actuel = actuel->suivant;
        }
    }
    afficher_liste (liste5);
}

//---------------------------------------
// DELAYED AIRLINE
//---------------------------------------
float delayed_airline (struct cellflight *tab_flight[], char *airlineid, int print)
{
    float somme = 0;
    float cpt   = 0;
    for (int i = 0; i < NBR_DATE; i++) // on parcourt la table de hachage des vols
    {
        struct cellflight *actuel = tab_flight[i];
        while (actuel != NULL)
        {
            if (strcmp (actuel->vol->airline, airlineid) == 0)
            {
                somme += actuel->vol->arr_delay;
                cpt += 1;
            }
            actuel = actuel->suivant;
        }
    }
    if (print == 1)
    {
        printf ("%s : %.2f minutes \n", airlineid, somme / cpt);
    }
    return somme / cpt;
}

//---------------------------------------
// MOST DELAYED AIRLINES
//---------------------------------------
void insertion_triee_airline (struct cellflight *tab_flight[], struct cellairline **pl, struct airline *a) // ajout dans une liste chainée dans l'ordre croissant
{
    if ((*pl == NULL) || (delayed_airline (tab_flight, a->iata_code, 0) <
                          delayed_airline (tab_flight, (*pl)->airl->iata_code, 0)))
    {
        ajout_tete_airline (pl, a);
    }
    else
    {
        insertion_triee_airline (tab_flight, &(*pl)->suivant, a);
    }
}

void afficher_mda (struct cellairline *l, struct cellflight *tab_flight[])
{
    while (l != NULL)
    {
        // on affiche les données de la compagnie + son temps de retard moyen
        printf ("%s, %s : %.2f minutes \n", l->airl->iata_code, l->airl->airline,
                delayed_airline (tab_flight, l->airl->iata_code, 0));
        l = l->suivant;
    }
}

void most_delayed_airlines (struct cellflight *tab_flight[], struct airline **tab_air)
{
    struct cellairline *liste5 = NULL;
    for (int i = 0; i < NBR_AIRLINES; i++) // on parcourt la table d'airlines
    {
        insertion_triee_airline (tab_flight, &liste5, tab_air[i]); // on ajoute dans la liste résultat la compagnie dans l'odre croissant selon le retard moyen
        if (taille ((struct cellflight *)liste5) > 5)
        {
            supp_tete ((struct cellflight **)&liste5); // on supprime le premier, et donc le plus petit élément de la liste
        }
    }
    afficher_mda (liste5, tab_flight);
}
