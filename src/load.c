
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "load.h"

#define FLIGHT_NUMBER 58492
#define NB_VALUE_PER_LINE 14
#define NBR_DATE 365
#define NBR_LETTER 26
#define AIRPORT_NUMBER 322
#define AIRLINE_NUMBER 14

int hash_date (int month, int day)
{
    int res = 0;
    for (int i = 1; i < month; i++)
    {
        if (i == 2)
            res += 28;
        else if (i == 4 || i == 6 || i == 9 || i == 11)
            res += 30;
        else
            res += 31;
    }
    return res + day - 1;
}

char hash_airport (char *iata) { return *iata - 65; }

void ajout_tete_date (struct cellflight **pl, struct flight *vol)
{
    struct cellflight *ptr;
    ptr          = calloc (1, sizeof (struct cellflight));
    ptr->vol     = vol;
    ptr->suivant = *pl;
    *pl          = ptr;
}

void ajout_tete_airport (struct cellairport **pl, struct airport *aerop)
{
    struct cellairport *ptr;
    ptr           = calloc (1, sizeof (struct cellairport));
    ptr->aeroport = aerop;
    ptr->suivant  = *pl;
    *pl           = ptr;
}


void put_flight (struct cellflight **tab, struct flight *temp)
{
    int indice = hash_date (temp->month, temp->day);
    ajout_tete_date (&tab[indice], temp);
}

void put_airport (struct cellairport **tab, struct airport *temp)
{
    int indice = hash_airport (temp->iata_code);
    ajout_tete_airport (&tab[indice], temp);
}

void put_airline (struct airline **tab, struct airline *temp, int indice) { tab[indice] = temp; }

// stocke chaque variable dans la structure
void store_flights (int value_nb, char *value, struct flight *flight_list)
{

    if (value_nb == 0)
        flight_list->month = atoi (value);
    else if (value_nb == 1)
        flight_list->day = atoi (value);
    else if (value_nb == 2)
        flight_list->weekday = atoi (value);
    else if (value_nb == 3)
        strcpy (flight_list->airline, value);
    else if (value_nb == 4)
        strcpy (flight_list->org_air, value);
    else if (value_nb == 5)
        strcpy (flight_list->dest_air, value);
    else if (value_nb == 6)
        flight_list->sched_dep = atoi (value);
    else if (value_nb == 7)
        flight_list->dep_delay = atof (value);
    else if (value_nb == 8)
        flight_list->air_time = atof (value);
    else if (value_nb == 9)
        flight_list->dist = atoi (value);
    else if (value_nb == 10)
        flight_list->sched_arr = atoi (value);
    else if (value_nb == 11)
        flight_list->arr_delay = atof (value);
    else if (value_nb == 12)
        flight_list->diverted = atoi (value);
    else if (value_nb == 13)
        flight_list->cancelled = atoi (value);
}

void store_airports (int value_nb, char *value, struct airport *airport_list)
{

    if (value_nb == 0)
        strcpy (airport_list->iata_code, value);
    else if (value_nb == 1)
        strcpy (airport_list->airport, value);
    else if (value_nb == 2)
        strcpy (airport_list->city, value);
    else if (value_nb == 3)
        strcpy (airport_list->state, value);
    else if (value_nb == 4)
        strcpy (airport_list->country, value);
    else if (value_nb == 5)
        airport_list->lattitude = atof (value);
    else if (value_nb == 6)
        airport_list->longitude = atof (value);
}

void store_airlines (int value_nb, char *value, struct airline *airline_list)
{

    if (value_nb == 0)
        strcpy (airline_list->iata_code, value);
    else if (value_nb == 1)
        strcpy (airline_list->airline, value);
}

void load_date (struct cellflight **flight_list)
{

    // ouverture du fichier
    FILE *fp = fopen ("data/flights.csv", "r");
    if (fp == NULL)
        printf ("fichier flight inaccessible\n");
    else
        printf ("fichier flight ouvert\n");

    char * line = NULL, *mot = NULL, *pl = NULL, d[] = ",";
    int    actual_flight = 0, value_nb = 0;
    size_t len = 0;

    getline (&line, &len, fp); // on ignore la 1ere ligne
    free (line);

    // parcours du tableau
    while (actual_flight != FLIGHT_NUMBER)
    {
        struct flight *temp_flight = calloc (1, sizeof (struct flight));
        line                       = NULL;
        len                        = 0;
        getline (&line, &len, fp);
        pl = line;

        mot = strsep (&pl, d);
        // construction du vol
        while (mot != NULL)
        {
            if (strcmp (mot, "") == 0)
            {
                store_flights (value_nb, "0", temp_flight);
            }
            else
            {
                store_flights (value_nb, mot, temp_flight);
            }
            mot = strsep (&pl, d);
            value_nb++;
        }

        // ajout du vol a la table
        put_flight (flight_list, temp_flight);
        value_nb = 0;
        actual_flight++;
        free (line);
    }
    fclose (fp);
    printf ("Chargement flight termine\n\n");
}

void load_airports (struct cellairport **airport_list)
{

    // ouverture du fichier
    FILE *fp = fopen ("data/airports.csv", "r");
    if (fp == NULL)
        printf ("fichier airport inaccessible\n");
    else
        printf ("fichier airport ouvert\n");

    char * line = NULL, *mot = NULL, **pl = NULL, d[] = ",";
    int    actual_airport = 0, value_nb = 0;
    size_t len = 0;
    pl         = &line;

    getline (&line, &len, fp); // on ignore la 1ere ligne
    free (line);

    // parcours du tableau
    while (actual_airport != AIRPORT_NUMBER)
    {
        struct airport *temp_airport = calloc (1, sizeof (struct airport));
        line                         = NULL;
        len                          = 0;
        getline (&line, &len, fp);
        mot = strsep (pl, d);

        while (mot != NULL)
        {
            if (strcmp (mot, "") == 0)
            {
                store_airports (value_nb, "0", temp_airport);
            }
            else
            {
                store_airports (value_nb, mot, temp_airport);
            }
            mot = strsep (pl, d);
            value_nb++;
        }

        put_airport (airport_list, temp_airport);

        value_nb = 0;
        actual_airport++;
        free (line);
    }
    fclose (fp);
    printf ("Chargement airports termine\n\n");
}

void remplace (char *chaine) // remplace \n par \0
{
    int i = 0;
    while (chaine[i] != 0)
    {
        if (chaine[i] == '\n')
        {
            chaine[i] = '\0';
            break;
        }
        i++;
    }
}

void load_airline (struct airline **airline_list)
{

    // ouverture du fichier
    FILE *fp = fopen ("data/airlines.csv", "r");
    if (fp == NULL)
        printf ("fichier airlines inaccessible\n");
    else
        printf ("fichier airlines ouvert\n");

    char * line = NULL, *mot = NULL, **pl = NULL, d[] = ",";
    int    actual_airline = 0, value_nb = 0;
    size_t len = 0;
    pl         = &line;

    getline (&line, &len, fp); // on ignore la 1ere ligne
    free (line);

    // parcours du tableau
    while (actual_airline != AIRLINE_NUMBER)
    {
        struct airline *temp_airline = calloc (1, sizeof (struct airline));
        line                         = NULL;
        len                          = 0;
        getline (&line, &len, fp);
        remplace (line);
        mot = strsep (pl, d);

        while (mot != NULL)
        {
            if (strcmp (mot, "") == 0)
            {
                store_airlines (value_nb, "0", temp_airline);
            }
            else
            {
                store_airlines (value_nb, mot, temp_airline);
            }
            mot = strsep (pl, d);
            value_nb++;
        }

        put_airline (airline_list, temp_airline, actual_airline);

        value_nb = 0;
        actual_airline++;
        free (line);
    }
    fclose (fp);
    printf ("Chargement airlines termine\n\n");
}

void suppr_tete_flights (struct cellflight **pl)
{
    if ((*pl) == NULL) return;

    if ((*pl)->suivant == NULL)
    {
        struct cellflight *tmp = (*pl);
        (*pl)                  = NULL;
        free (tmp);
        return;
    }
    struct cellflight *tmp = *pl;
    *pl                    = (*pl)->suivant;
    free (tmp->vol);
    free (tmp);
}

void lib_flights (struct cellflight **pl)
{
    for (int i = 0; i < NBR_DATE; i++)
    {
        while ((pl[i]) != NULL)
        {
            suppr_tete_flights (&pl[i]);
        }
    }
}

void suppr_tete_airports (struct cellairport **pl)
{
    if ((*pl) == NULL) return;

    if ((*pl)->suivant == NULL)
    {
        struct cellairport *tmp = (*pl);
        (*pl)                   = NULL;
        free (tmp);
        return;
    }
    struct cellairport *tmp = *pl;
    *pl                     = (*pl)->suivant;
    free (tmp->aeroport);
    free (tmp);
}

void lib_airports (struct cellairport **pl)
{
    for (int i = 0; i < AIRPORT_NUMBER; i++)
    {
        while ((pl[i]) != NULL)
        {
            suppr_tete_airports (&pl[i]);
        }
    }
}
