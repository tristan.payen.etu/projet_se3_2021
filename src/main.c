#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "fonctions.h"
#include "load.h"

#define FLIGHT_NUMBER 58492
#define NB_VALUE_PER_LINE 14
#define NBR_DATE 365
#define NBR_LETTER 26
#define NBR_AIRLINES 14
#define BUFFER_SIZE 256

int main ()
{
    struct cellflight * dayList[NBR_DATE]         = { 0 }; // table de hashage
    struct cellairport *airportList[NBR_LETTER]   = { 0 };
    struct airline *    airlineList[NBR_AIRLINES] = { 0 };

    // Chargement des tables
    load_date (dayList);
    load_airports (airportList);
    load_airline (airlineList);

    char buffer[BUFFER_SIZE] = { 0 };
    int  mois, jour, limit, time;
    char port1[BUFFER_SIZE], port2[BUFFER_SIZE], airline[BUFFER_SIZE];

    // lecture des commandes
    for (;;)
    {
        char *res = fgets (buffer, sizeof (buffer), stdin);
        // interruption de la boucle
        if (res == NULL)
        {
            break;
        }
        else if (strcmp (buffer, "quit\n") == 0)
        {
            break;
        }
        // requêtes
        else if (sscanf (buffer, "changed-flights %d-%d", &mois, &jour) == 2)
        {
            changed_flights (dayList, mois, jour);
            printf ("\n");
        }
        else if (strcmp (buffer, "most-delayed-flights\n") == 0)
        {
            most_delayed_flights (dayList);
            printf ("\n");
        }
        else if (sscanf (buffer, "avg-flight-duration %s %s", port1, port2) == 2)
        {
            avg_flight_duration (dayList, port1, port2);
            printf ("\n");
        }
        else if (sscanf (buffer, "show-airports %s", airline) == 1)
        {
            show_airports (dayList, airportList, airline);
            printf ("\n");
        }
        else if (sscanf (buffer, "show-airlines %s", port1) == 1)
        {
            show_airlines (dayList, airlineList, port1);
            printf ("\n");
        }
        else if (sscanf (buffer, "show-flights %s %d-%d %d limit=%d", port1, &mois, &jour, &time, &limit) == 5)
        {
            show_flights (dayList, port1, mois, jour, time, limit);
            printf ("\n");
        }
        else if (sscanf (buffer, "delayed-airline %s", airline) == 1)
        {
            delayed_airline (dayList, airline, 1);
            printf ("\n");
        }
        else if (strcmp (buffer, "most-delayed-airlines\n") == 0)
        {
            most_delayed_airlines (dayList, airlineList);
            printf ("\n");
        }
    }
    lib_flights (dayList);
    lib_airports (airportList);

    return 0;
}
